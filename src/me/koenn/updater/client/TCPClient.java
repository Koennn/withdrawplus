package me.koenn.updater.client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class TCPClient {

    private static String ip = "37.187.207.177";

    /**
     * Send a TCP message to the VersionServer.
     *
     * @param message Message to send
     * @return Response from the server
     * @throws IOException
     */
    public static String sendToServer(String message) throws IOException {
        //Connect to the VersionServer.
        Socket clientSocket = new Socket(ip, 9096);

        //Make the input and output variables.
        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        //Send the message to the server.
        outToServer.writeBytes(message + '\n');
        outToServer.flush();

        //Read the response of the server.
        String response = inFromServer.readLine();

        //Close the connection.
        clientSocket.close();

        //Return the response of the server.
        return response;
    }
}
