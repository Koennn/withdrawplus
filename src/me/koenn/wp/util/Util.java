package me.koenn.wp.util;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * General utility class for all kinds of utility stuff.
 */
public class Util {

    /**
     * Get the index of the ExpVoucher value in the config.
     *
     * @return int index
     */
    public static int getAmountIndexForXpVoucherString() {
        String[] lore = ConfigManager.getList("lore", "expVoucher").get(getAmountIndexForXpVoucher()).split(" ");
        for (int i = 0; i < lore.length; i++) {
            if (lore[i].contains("{amount}")) {
                return i;
            }
        }
        throw new NullPointerException("Lore does not contain an amount");
    }

    /**
     * Get the index of the MoneyVoucher value in the config.
     *
     * @return int index
     */
    public static int getAmountIndexForMoneyVoucherString() {
        String[] lore = ConfigManager.getList("lore", "moneyVoucher").get(getAmountIndexForMoneyVoucher()).split(" ");
        for (int i = 0; i < lore.length; i++) {
            if (lore[i].contains("{amount}")) {
                return i;
            }
        }
        throw new NullPointerException("Lore does not contain an amount");
    }

    /**
     * Get the line index of the MoneyVoucher value in the config.
     *
     * @return int index
     */
    public static int getAmountIndexForMoneyVoucher() {
        List<String> lore = ConfigManager.getList("lore", "moneyVoucher");
        for (int i = 0; i < lore.size(); i++) {
            if (lore.get(i).contains("{amount}")) {
                return i;
            }
        }
        throw new NullPointerException("Lore does not contain an amount");
    }

    /**
     * Get the line index of the ExpVoucher value in the config.
     *
     * @return int index
     */
    public static int getAmountIndexForXpVoucher() {
        List<String> lore = ConfigManager.getList("lore", "expVoucher");
        for (int i = 0; i < lore.size(); i++) {
            if (lore.get(i).contains("{amount}")) {
                return i;
            }
        }
        throw new NullPointerException("Lore does not contain an amount");
    }

    /**
     * Remove one of the items in the player's hand.
     *
     * @param player Player object instance
     */
    public static void removeItemInHand(Player player) {
        if (getItemInHand(player).getAmount() > 1) {
            getItemInHand(player).setAmount(getItemInHand(player).getAmount() - 1);
        } else {
            setItemInHand(player, new ItemStack(Material.AIR));
        }
    }

    /**
     * Get the item in the player's hand.
     *
     * @param player Player object instance
     * @return ItemStack item in hand
     */
    @SuppressWarnings("deprecation")
    public static ItemStack getItemInHand(Player player) {
        if (Bukkit.getServer().getVersion().contains("MC: 1.9")) {
            return player.getInventory().getItemInMainHand();
        } else {
            return player.getItemInHand();
        }
    }

    /**
     * Set the item in the player's hand.
     *
     * @param player Player object instance
     * @param item   ItemStack item to put in player's hand
     */
    @SuppressWarnings("deprecation")
    public static void setItemInHand(Player player, ItemStack item) {
        if (Bukkit.getServer().getVersion().contains("MC: 1.9")) {
            player.getInventory().setItemInMainHand(item);
        } else {
            player.setItemInHand(item);
        }
    }
}