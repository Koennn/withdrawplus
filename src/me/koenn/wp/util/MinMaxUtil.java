package me.koenn.wp.util;

/**
 * Getters for the minimum and maximum values of the vouchers.
 */
public class MinMaxUtil {

    public static int getMaxExp() {
        return Integer.parseInt(ConfigManager.getString("MAX_EXP", "minMaxValues"));
    }

    public static int getMinExp() {
        return Integer.parseInt(ConfigManager.getString("MIN_EXP", "minMaxValues"));
    }

    public static int getMaxMoney() {
        return Integer.parseInt(ConfigManager.getString("MAX_MONEY", "minMaxValues"));
    }

    public static int getMinMoney() {
        return Integer.parseInt(ConfigManager.getString("MIN_MONEY", "minMaxValues"));
    }
}
