package me.koenn.wp.commands;

import org.bukkit.command.CommandSender;

/**
 * WithdrawPlus command interface.
 */
public interface WithdrawCommand {

    /**
     * Execute the command.
     *
     * @param player CommandSender object instance
     * @param args   command arguments
     * @return boolean handled
     */
    boolean execute(CommandSender player, String[] args);

    /**
     * Get the name of the command.
     *
     * @return String name
     */
    String getName();

    /**
     * Get the correct usage of the command.
     *
     * @return String usage
     */
    String getUsage();
}
