package me.koenn.wp;

import me.koenn.updater.client.Updater;
import me.koenn.wp.commands.CommandHandler;
import me.koenn.wp.listeners.CommandListener;
import me.koenn.wp.listeners.PlayerInteractListener;
import me.koenn.wp.util.ConfigManager;
import me.koenn.wp.util.ItemUtil;
import me.koenn.wp.util.SoundUtil;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * WithdrawPlus plugin.
 * A customizable withdraw plugin for Money and Exp.
 *
 * @author Koenn
 */
public class WithdrawPlus extends JavaPlugin {

    public static Economy econ = null;
    private static CommandHandler commandHandler;
    private static JavaPlugin plugin;

    /**
     * Bukkit version. Can be either 1.8, 1.9 or 1.10
     */
    private static double bukkitVersion;

    /**
     * Get the version of Bukkit that's on the server.
     *
     * @return double version
     */
    public static double getBukkitVersion() {
        return bukkitVersion;
    }

    /**
     * Get the current JavaPlugin instance.
     *
     * @return JavaPlugin instance.
     */
    public static JavaPlugin getPlugin() {
        return plugin;
    }

    /**
     * Get the current CommandHandler instance.
     *
     * @return CommandHandler instance
     */
    public static CommandHandler getCommandHandler() {
        return commandHandler;
    }

    @Override
    public void onEnable() {
        //Set static variables.
        plugin = this;

        //Print startup message
        log("All credits for this plugin go to Koenn");

        //Get the current Bukkit version.
        this.getVersion();

        //Check for Vault dependency.
        if (!(setupEconomy())) {
            this.getLogger().severe("Disabled due to no Vault dependency found!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
            return;
        }

        //Setup ConfigManager.
        ConfigManager configManager = new ConfigManager(this.getConfig(), this);
        configManager.setupConfig();

        //Setup items and sounds.
        ItemUtil.setupMaterials();
        SoundUtil.setupSounds();

        //Register Bukkit event listeners.
        Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new CommandListener(), this);

        //Setup command handler.
        commandHandler = new CommandHandler();
        getCommand("withdrawXp").setExecutor(commandHandler);
        getCommand("withdrawMoney").setExecutor(commandHandler);
        getCommand("givevoucher").setExecutor(commandHandler);
        commandHandler.setup();

        //Start the Updater.
        if (Boolean.parseBoolean(ConfigManager.getString("autoUpdate", "misc").toUpperCase())) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(this, () -> new Updater(this, "https://www.spigotmc.org/resources/withdrawplus.18447/", "18447"), 20);
        }
    }

    private void getVersion() {
        //Check the version and set the bukkitVersion variable.
        String versionString = Bukkit.getServer().getBukkitVersion();
        if (versionString.contains("1.9")) {
            bukkitVersion = 1.9;
        } else if (versionString.contains("1.8")) {
            bukkitVersion = 1.8;
        } else if (versionString.contains("1.10")) {
            bukkitVersion = 1.10;
        } else {
            log("");
            log("This plugin is only compatible with 1.8, 1.9 and 1.10");
            log("");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        log("Loading plugin for minecraft version " + versionString + "!");
    }

    @Override
    public void onDisable() {
        log("All credits for this plugin go to Koenn");
    }

    /**
     * Static logger method.
     *
     * @param msg Message to log to the console
     */
    private void log(String msg) {
        this.getLogger().info(msg);
    }

    /**
     * Loads the Economy of the server in the static economy variable.
     *
     * @return boolean succeed
     * @see Economy
     */
    private boolean setupEconomy() {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
}